from django.urls import path
from .views import (
    project_list,
    show_project,
    create_project,
)

app_name = "projects"

urlpatterns = [
    path("", project_list, name="home"),
    path("create/", create_project, name="create_project"),
    path("<int:id>/", show_project, name="show_project"),
]
