from django.contrib import admin
from .models import (
    Team,
    TeamProject,
    TeamMember,
)


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
    )


@admin.register(TeamProject)
class TeamProjectAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
        'team',
    )


@admin.register(TeamMember)
class TeamMemberAdmin(admin.ModelAdmin):
    list_display = (
        'first_name',
        'last_name',
        'username',
        'email',
        'role',
    )
