from django.urls import path
from django.shortcuts import redirect
from .views import (
    team_list,
    create_team,
    show_my_teams,
    create_team_project,
    create_team_member,
)

app_name = "teams"


def my_teams_redirect_view(request):
    return redirect("teams:list_teams")


urlpatterns = [
    path("", team_list, name='list_teams'),
    path("create/", create_team, name='create_team'),
    path('<int:team_id>/', show_my_teams, name='show_my_teams'),
    path('<int:team_id>/create_team_project/', create_team_project,
         name='create_project'),
    path('<int:team_id>/create_team_member/', create_team_member,
         name='add_member'),
]
