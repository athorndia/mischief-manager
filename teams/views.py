from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import Team, TeamMember
from .forms import (
    TeamForm,
    TeamProjectForm,
    TeamMemberForm,
)


@login_required
def team_list(request):
    if request.method == 'POST':
        form = TeamForm(request.POST)
        if form.is_valid():
            team = form.save(commit=False)
            team.save()
            team.members.add(request.user)
            return redirect("teams_home")
    else:
        form = TeamForm()
    teams = Team.objects.all()
    context = {
        'teams': teams,
        'form': form
        }
    return render(request, 'teams/list.html', context)


@login_required
def create_team(request):
    if request.method == 'POST':
        form = TeamForm(request.POST)
        if form.is_valid():
            team = form.save(commit=False)
            team.owner = request.user
            team.save()
            team.members.add(request.user)
            return redirect('list_teams', team_id=team.id)
    else:
        form = TeamForm()
    context = {'form': form}
    return render(request, 'teams/create.html', context)


@login_required
def show_my_teams(request, team_id):
    team = get_object_or_404(Team, id=team_id)
    context = {
        'team': team
        }
    return render(request, 'teams/detail.html', context)


@login_required
def create_team_project(request, team_id):
    team = get_object_or_404(Team, id=team_id)
    if request.method == 'POST':
        form = TeamProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.team = team
            project.save()
            project.members.add(request.user)
            return redirect('show_team', team_id=team.id)
    else:
        form = TeamProjectForm()
    context = {
        'form': form
        }
    return render(request, 'teams/create_team_project.html', context)


def create_team_member(request):
    if request.method == 'POST':
        form = TeamMemberForm(request.POST)
        if form.is_valid():
            # Create a new TeamMember object with the submitted data
            member = TeamMember(
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name'],
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email'],
                role=form.cleaned_data['role']
            )
            member.save()
            return redirect('show_team', member_id=member.id)
    else:
        form = TeamMemberForm()
    context = {
        'form': form
    }
    return render(request, 'teams/create_team_member.html', context)
