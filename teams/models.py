from django.db import models
from django.contrib.auth.models import User
from projects.models import Project


class Team(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(
        max_length=200,
        default=None,
        )
    members = models.ManyToManyField(User)
    projects = models.ManyToManyField(Project)

    def __str__(self):
        return self.name


class TeamProject(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(
        max_length=200,
        default=None,
        )
    team = models.ForeignKey(
        Team,
        on_delete=models.CASCADE,
        related_name="team_projects",
        null=True,
        )

    def __str__(self):
        return self.name


class TeamMember(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    email = models.EmailField(max_length=200)
    role = models.CharField(max_length=100)

    def __str__(self):
        return self.username
